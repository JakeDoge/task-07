package com.epam.rd.java.basic.task7.db.entity;

public class User {

    private int id;

    private String login;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public static User createUser(String login) {
        User newUser = new User();
        newUser.setId(0);
        newUser.setLogin(login);
        return newUser;
    }

    @Override
    public String toString() {
        return "User[" +
                "id:" + id +
                ", login:'" + login + '\'' + ']';
    }

    @Override
    public int hashCode() {
        return login != null ? login.hashCode() : 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        User user = (User) obj;

        return login != null ? login.equals(user.login) : user.login == null;
    }



}
